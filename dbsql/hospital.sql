/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50714
Source Host           : localhost:3306
Source Database       : hospital

Target Server Type    : MYSQL
Target Server Version : 50714
File Encoding         : 65001

Date: 2018-01-17 15:25:14
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `appointment`
-- ----------------------------
DROP TABLE IF EXISTS `appointment`;
CREATE TABLE `appointment` (
  `id` int(50) NOT NULL AUTO_INCREMENT,
  `doctor_id` int(50) DEFAULT NULL,
  `patient_id` int(50) DEFAULT NULL,
  `created` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of appointment
-- ----------------------------

-- ----------------------------
-- Table structure for `doctors`
-- ----------------------------
DROP TABLE IF EXISTS `doctors`;
CREATE TABLE `doctors` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(120) DEFAULT NULL,
  `cell` varchar(25) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `appointment` varchar(255) DEFAULT NULL,
  `specialist` varchar(255) DEFAULT NULL,
  `daysInWeek` varchar(255) DEFAULT NULL,
  `created` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doctors
-- ----------------------------
INSERT INTO `doctors` VALUES ('1', 'doctor', 'docto@gmail.com', '1232', 'fdfdfd', 'fdfdfd', 'cardiologist', 'ssndfd', '2018-01-11 17:17:45.000000', '2018-01-12 06:04:43.000000', '1.jpg');
INSERT INTO `doctors` VALUES ('2', 'asdfsld', 'asd@gmail.com', '1254221', 'dfdfdfd', 'dfdfdfd', 'cardiologist', 'sdfdks lgkd', '2018-01-11 18:38:45.000000', '2018-01-11 18:38:45.000000', '2.jpg');
INSERT INTO `doctors` VALUES ('3', 'doctor', 'docto@gmail.com', '1232', 'fdfdfd', 'fdfdfd', 'dentist', 'ssndfd', '2018-01-11 18:39:13.000000', '2018-01-11 18:39:13.000000', '3.jpg');
INSERT INTO `doctors` VALUES ('4', 'doctor', 'docto@gmail.com', '1232', 'fdfdfd', 'fdfdfd', 'neurologist', 'ssndfd', '2018-01-11 18:44:50.000000', '2018-01-11 18:44:50.000000', '4.jpg');
INSERT INTO `doctors` VALUES ('5', 'doctor', 'docto@gmail.com', '1232', 'fdfdfd', 'fdfdfd', 'psychologist', 'ssndfd', '2018-01-11 18:48:14.000000', '2018-01-11 18:48:14.000000', '1515754572333.jpg');
INSERT INTO `doctors` VALUES ('6', 'doctor', 'docto@gmail.com', '1232', 'fdfdfd', 'fdfdfd', 'psychologist', 'ssndfd', '2018-01-12 05:45:48.000000', '2018-01-12 05:45:48.000000', '1.jpg');
INSERT INTO `doctors` VALUES ('7', 'dfasdfas', 'as@gmail.com', '1254221', 'khdfsgk', 'sdfd', 'dentist', 'fdkfjd', '2018-01-12 10:34:57.000000', '2018-01-12 10:34:57.000000', '2.jpg');
INSERT INTO `doctors` VALUES ('8', 'dfasdfas', 'as@gmail.com', '1254221', 'khdfsgk', 'sdfd', 'dentist', 'fdkfjd', '2018-01-12 10:46:30.000000', '2018-01-12 10:46:30.000000', '3.jpg');
INSERT INTO `doctors` VALUES ('9', 'dfasdfas', 'as@gmail.com', '1254221', 'khdfsgk', 'sdfd', 'dentist', 'fdkfjd', '2018-01-12 10:52:27.000000', '2018-01-12 10:52:27.000000', '4.jpg');
INSERT INTO `doctors` VALUES ('10', 'dfasdfas', 'as@gmail.com', '1254221', 'khdfsgk', 'sdfd', 'dentist', 'fdkfj', '2018-01-12 10:56:15.000000', '2018-01-12 12:39:01.000000', '1.jpg');
INSERT INTO `doctors` VALUES ('11', 'fjkldfs', 'as@gmail.com', '1254665', 'dfdfd', 'fdfdfd', 'dserdfd', 'sdfdfd', '2018-01-12 11:58:40.000000', '2018-01-12 12:14:19.000000', '1515755659592.jpg');

-- ----------------------------
-- Table structure for `operators`
-- ----------------------------
DROP TABLE IF EXISTS `operators`;
CREATE TABLE `operators` (
  `id` int(10) NOT NULL,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `cell` varchar(25) DEFAULT NULL,
  `age` int(6) DEFAULT NULL,
  `sex` varchar(10) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `created` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of operators
-- ----------------------------

-- ----------------------------
-- Table structure for `patients`
-- ----------------------------
DROP TABLE IF EXISTS `patients`;
CREATE TABLE `patients` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(120) DEFAULT NULL,
  `cell` varchar(25) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `doctor_name` varchar(100) DEFAULT NULL,
  `appointmentDate` datetime(6) DEFAULT NULL,
  `problem` varchar(255) DEFAULT NULL,
  `age` int(10) DEFAULT NULL,
  `sex` varchar(10) DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL,
  `created` datetime(6) DEFAULT NULL,
  `modified` datetime(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of patients
-- ----------------------------

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
