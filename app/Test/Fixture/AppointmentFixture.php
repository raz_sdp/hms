<?php
/**
 * Appointment Fixture
 */
class AppointmentFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'appointment';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 50, 'unsigned' => false, 'key' => 'primary'),
		'doctor_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 50, 'unsigned' => false),
		'patient_id' => array('type' => 'integer', 'null' => true, 'default' => null, 'length' => 50, 'unsigned' => false),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null, 'length' => 6),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null, 'length' => 6),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_general_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'doctor_id' => 1,
			'patient_id' => 1,
			'created' => '2018-01-13 07:04:04',
			'modified' => '2018-01-13 07:04:04'
		),
	);

}
