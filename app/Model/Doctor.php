<?php
App::uses('AppModel', 'Model');
/**
 * Doctor Model
 *
 */
class Doctor extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $displayField = 'name';

}
