
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Patients</h3>
                    </div>


                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
															<th><?php echo $this->Paginator->sort('id'); ?></th>
															<th><?php echo $this->Paginator->sort('name'); ?></th>
															<th><?php echo $this->Paginator->sort('email'); ?></th>
															<th><?php echo $this->Paginator->sort('cell'); ?></th>
															<th><?php echo $this->Paginator->sort('address'); ?></th>
															<th><?php echo $this->Paginator->sort('doctor_name'); ?></th>
															<th><?php echo $this->Paginator->sort('appointmentDate'); ?></th>
															<th><?php echo $this->Paginator->sort('problem'); ?></th>
															<th><?php echo $this->Paginator->sort('age'); ?></th>
															<th><?php echo $this->Paginator->sort('sex'); ?></th>
															<th><?php echo $this->Paginator->sort('status'); ?></th>
															<th><?php echo $this->Paginator->sort('created'); ?></th>
															<th><?php echo $this->Paginator->sort('modified'); ?></th>
															<th class="actions"><?php echo __('Actions'); ?></th>
                            </tr>
                            </thead>
                            <tbody>
															<?php foreach ($patients as $patient): ?>
															<tr>
																<td><?php echo h($patient['Patient']['id']); ?>&nbsp;</td>
																<td><?php echo h($patient['Patient']['name']); ?>&nbsp;</td>
																<td><?php echo h($patient['Patient']['email']); ?>&nbsp;</td>
																<td><?php echo h($patient['Patient']['cell']); ?>&nbsp;</td>
																<td><?php echo h($patient['Patient']['address']); ?>&nbsp;</td>
																<td><?php echo h($patient['Patient']['doctor_name']); ?>&nbsp;</td>
																<td><?php echo h($patient['Patient']['appointmentDate']); ?>&nbsp;</td>
																<td><?php echo h($patient['Patient']['problem']); ?>&nbsp;</td>
																<td><?php echo h($patient['Patient']['age']); ?>&nbsp;</td>
																<td><?php echo h($patient['Patient']['sex']); ?>&nbsp;</td>
																<td><?php echo h($patient['Patient']['status']); ?>&nbsp;</td>
																<td><?php echo h($patient['Patient']['created']); ?>&nbsp;</td>
																<td><?php echo h($patient['Patient']['modified']); ?>&nbsp;</td>
																<td class="actions">
																	<?php echo $this->Html->link(__('View'), array('action' => 'view', $patient['Patient']['id'])); ?>
																	<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $patient['Patient']['id'])); ?>
																	<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $patient['Patient']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $patient['Patient']['id']))); ?>
																</td>
															</tr>
														<?php endforeach; ?>

                            </tbody>

                        </table>
                        <p class="pull-right">
													<?php
													echo $this->Paginator->counter(array(
														'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
													)); ?>
                        </p>
                        <div class="clearfix"></div>
                        <ul class="pagination pagination-sm no-margin pull-right">
													<?php
														echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
														echo $this->Paginator->numbers(array('separator' => ''));
														echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
													?>
                        </ul>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->


                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
