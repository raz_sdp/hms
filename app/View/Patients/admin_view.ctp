<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Patient Details</h3>
                    </div>
                    <!-- /.box-header -->



                    <!-- /.Search Box -->
										<div class="patients view">
										<h2><?php echo __('Patient'); ?></h2>
											<dl>
												<dt><?php echo __('Id'); ?></dt>
												<dd>
													<?php echo h($patient['Patient']['id']); ?>
													&nbsp;
												</dd>
												<dt><?php echo __('Name'); ?></dt>
												<dd>
													<?php echo h($patient['Patient']['name']); ?>
													&nbsp;
												</dd>
												<dt><?php echo __('Email'); ?></dt>
												<dd>
													<?php echo h($patient['Patient']['email']); ?>
													&nbsp;
												</dd>
												<dt><?php echo __('Cell'); ?></dt>
												<dd>
													<?php echo h($patient['Patient']['cell']); ?>
													&nbsp;
												</dd>
												<dt><?php echo __('Address'); ?></dt>
												<dd>
													<?php echo h($patient['Patient']['address']); ?>
													&nbsp;
												</dd>
												<dt><?php echo __('Doctor Name'); ?></dt>
												<dd>
													<?php echo h($patient['Patient']['doctor_name']); ?>
													&nbsp;
												</dd>
												<dt><?php echo __('AppointmentDate'); ?></dt>
												<dd>
													<?php echo h($patient['Patient']['appointmentDate']); ?>
													&nbsp;
												</dd>
												<dt><?php echo __('Problem'); ?></dt>
												<dd>
													<?php echo h($patient['Patient']['problem']); ?>
													&nbsp;
												</dd>
												<dt><?php echo __('Age'); ?></dt>
												<dd>
													<?php echo h($patient['Patient']['age']); ?>
													&nbsp;
												</dd>
												<dt><?php echo __('Sex'); ?></dt>
												<dd>
													<?php echo h($patient['Patient']['sex']); ?>
													&nbsp;
												</dd>
												<dt><?php echo __('Status'); ?></dt>
												<dd>
													<?php echo h($patient['Patient']['status']); ?>
													&nbsp;
												</dd>
												<dt><?php echo __('Created'); ?></dt>
												<dd>
													<?php echo h($patient['Patient']['created']); ?>
													&nbsp;
												</dd>
												<dt><?php echo __('Modified'); ?></dt>
												<dd>
													<?php echo h($patient['Patient']['modified']); ?>
													&nbsp;
												</dd>
											</dl>
										</div>
										<div class="actions">
											<h3><?php echo __('Actions'); ?></h3>
											<ul>
		<li><?php echo $this->Html->link(__('Edit Patient'), array('action' => 'edit', $patient['Patient']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Patient'), array('action' => 'delete', $patient['Patient']['id']), array('confirm' => __('Are you sure you want to delete # %s?', $patient['Patient']['id']))); ?> </li>
		
	</ul>
										</div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->


                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
