
<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Patient Details</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <!--<form role="form">-->
                    <?php echo $this->Form->create('Patient',array('type'=>'file'));?>
                        <div class="box-body">
                            <div class="form-group">

                                <?php  echo $this->Form->input('name',array('class'=>'form-control','placeholder'=>'Fullname'));?>

                            </div>
                            <div class="form-group">
                                <?php  echo $this->Form->input('email',array('class'=>'form-control','placeholder'=>'Email'));?>
                            </div>
														<div class="form-group">
                                <?php  echo $this->Form->input('cell',array('class'=>'form-control','placeholder'=>'Mobile'));?>
                            </div>
														<div class="form-group">
                                <?php  echo $this->Form->input('address',array('class'=>'form-control','placeholder'=>'Address'));?>
                            </div>
                            <div class="form-group">
                                <?php  echo $this->Form->input('doctor_name',array('class'=>'form-control','placeholder'=>'Name of the Doctor'));?>
                            </div>
                            <div class="form-group">
                                <?php  echo $this->Form->input('appointmentDate',array('class'=>'form-control','placeholder'=>'appointment date'));?>
                            </div>
                            <div class="form-group">

                                <?php  echo $this->Form->input('problem',array('class'=>'form-control','placeholder'=>'problems'));?>
                            </div>

                            <div class="form-group">

                                <?php  echo $this->Form->input('age',array('class'=>'form-control','placeholder'=>'Age'));?>
                            </div>
                            <div class="form-group">
                                <?php  echo $this->Form->input('sex',array('class'=>'form-control','placeholder'=>'Gender'));?>
                            </div>
                            <div class="form-group">

                                <?php  echo $this->Form->input('status',array('class'=>'form-control','placeholder'=>'Status'));?>
                            </div>

                    <?php echo $this->Form->end(__('Submit',array('class'=>'btn btn-primary'))); ?>
                   <!-- </form>---->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
						<!--<div class="actions">
							<h3><?php echo __('Actions'); ?></h3>
							<ul>
								<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Doctor.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('Doctor.id')))); ?></li>
								<li><?php echo $this->Html->link(__('List Doctors'), array('action' => 'index')); ?></li>
							</ul>
						</div> -->
        </div>
        <!--/.col (right) -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
