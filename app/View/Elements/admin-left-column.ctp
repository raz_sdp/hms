<aside class="main-sidebar">
<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">
<!-- Sidebar user panel -->
<div class="user-panel">
    <div class="pull-left image">
        <!-- <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">-->
        <?php echo $this->Html->image('user2-160x160.jpg', ['class' => 'img-circle', 'alt' => 'User Image']) ?>
    </div>
    <div class="pull-left info">
        <p>Alexander Pierce</p>
        <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
    </div>
</div>

<!-- sidebar menu: : style can be found in sidebar.less -->
<ul class="sidebar-menu" data-widget="tree">
<li class="header">MAIN NAVIGATION</li>


<!--/Manage Dashboard-->
<?php
//pr($this->params);die;
$ctrl = strtolower($this->params['controller']);
$action = strtolower($this->params['action']);
$role = AuthComponent::user()['role'];
?>

<!--Manage Doctors-->
<li class="treeview <?php echo $ctrl=='doctors' ? 'active menu-open' : ''?>">
    <a href="#">
        <i class="fa fa-users"></i> <span>Manage Doctors</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo $ctrl=='doctors' && $action=='admin_add'? 'active' : ''?>"><a href="<?php echo $this->Html->url("/admin/doctors/add", true) ?>"><i
                    class="fa fa-user-plus"></i> Add New Doctor</a></li>
        <li class="<?php echo $ctrl=='doctors' && $action=='admin_index'? 'active' : ''?>"><a href="<?php echo $this->Html->url("/admin/doctors/index", true) ?>"><i
                    class="fa fa-list"></i>List Doctors</a></li>
    </ul>
</li>
<!--/Manage Doctors-->


<!-- Manage Patients  -->
<li class="treeview <?php echo $ctrl=='patients' ? 'active menu-open' : ''?> ">
    <a href="#">
        <i class="fa  fa-users"></i> <span>Manage Patients</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo $ctrl=='patients'&& $action=='admin_add' ? 'active' : ''?>"><a href="<?php echo $this->Html->url("/admin/patients/add", true) ?>"><i
                    class="fa fa-plus"></i> Add New Patient</a></li>
        <li class="<?php echo $ctrl=='patients' && $action=='admin_index'? 'active' : ''?>"><a href="<?php echo $this->Html->url("/admin/patients/index", true) ?>"><i
                    class="fa fa-list"></i>List All Pateients</a></li>
    </ul>
</li>
<!-- /Manages patients-->

<!-- Manage admins  -->
<?php if($role != 'doctor'): ?>
<li class="treeview <?php echo $ctrl=='users' ? 'active menu-open' : ''?> ">
    <a href="#">
        <i class="fa  fa-users"></i> <span>Manage Admins</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
    </a>
    <ul class="treeview-menu">
        <li class="<?php echo $ctrl=='users'&& $action=='admin_add' ? 'active' : ''?>"><a href="<?php echo $this->Html->url("/admin/users/add", true) ?>"><i
                    class="fa fa-plus"></i> Add New Admin</a></li>
        <li class="<?php echo $ctrl=='users' && $action=='admin_index'? 'active' : ''?>"><a href="<?php echo $this->Html->url("/admin/users/index", true) ?>"><i
                    class="fa fa-list"></i>List All Admins</a></li>
    </ul>
</li>
<?php endif; ?>
<!-- /Manages admins-->





</section>
<!-- /.sidebar -->
</aside>
