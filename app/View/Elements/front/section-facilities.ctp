<section id="facilities" class="home-section paddingbot-60">
  <div class="container marginbot-50">
    <div class="row">
      <div class="col-lg-8 col-lg-offset-2">
        <div class="wow fadeInDown" data-wow-delay="0.1s">
          <div class="section-heading text-center">
            <h2 class="h-bold">Our facilities</h2>
            <p>Ea melius ceteros oportere quo, pri habeo viderer facilisi ei</p>
          </div>
        </div>
        <div class="divider-short"></div>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-sm-12 col-md-12 col-lg-12">
        <div class="wow bounceInUp" data-wow-delay="0.2s">
          <div id="owl-works" class="owl-carousel">
            <div class="item">
              <a href="<?php echo $this->Html->url('/img/front/photo/1.jpg',true);?>" title="This is an image title" data-lightbox-gallery="gallery1" data-lightbox-hidpi="<?php echo $this->Html->url('/img/front/works/1@2x.jpg',true);?>">
                <?php echo $this->Html->image('front/photo/1.jpg',['class'=>'img-responsive','alt'=>'img']);?>
              </a>
            </div>
            <div class="item">
              <a href="<?php echo $this->Html->url('/img/front/photo/2.jpg',true);?>" title="This is an image title" data-lightbox-gallery="gallery1" data-lightbox-hidpi="<?php echo $this->Html->url('/img/front/works/2@2x.jpg',true);?>">
              <?php echo $this->Html->image('front/photo/2.jpg',['class'=>'img-responsive','alt'=>'img']);?>
              </a>
            </div>
            <div class="item">
              <a href="<?php echo $this->Html->url('/img/front/photo/3.jpg',true);?>" title="This is an image title" data-lightbox-gallery="gallery1" data-lightbox-hidpi="<?php echo $this->Html->url('/img/front/works/3@2x.jpg',true);?>">
              <?php echo $this->Html->image('front/photo/3.jpg',['class'=>'img-responsive','alt'=>'img']);?>
              </a>
            </div>
            <div class="item">
              <a href="<?php echo $this->Html->url('/img/front/photo/4.jpg',true);?>" title="This is an image title" data-lightbox-gallery="gallery1" data-lightbox-hidpi="<?php echo $this->Html->url('/img/front/works/4@2x.jpg',true);?>">
              <?php echo $this->Html->image('front/photo/4.jpg',['class'=>'img-responsive','alt'=>'img']);?>
              </a>
            </div>
            <div class="item">
              <a href="<?php echo $this->Html->url('/img/front/photo/5.jpg',true);?>" title="This is an image title" data-lightbox-gallery="gallery1" data-lightbox-hidpi="<?php echo $this->Html->url('/img/front/works/5@2x.jpg',true);?>">
              <?php echo $this->Html->image('front/photo/5.jpg',['class'=>'img-responsive','alt'=>'img']);?>
              </a>
            </div>
            <div class="item">
              <a href="<?php echo $this->Html->url('/img/front/photo/6.jpg',true);?>" title="This is an image title" data-lightbox-gallery="gallery1" data-lightbox-hidpi="<?php echo $this->Html->url('/img/front/works/6@2x.jpg',true);?>">
              <?php echo $this->Html->image('front/photo/6.jpg',['class'=>'img-responsive','alt'=>'img']);?>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- /Section: works -->


<!-- Section: testimonial -->
<section id="testimonial" class="home-section paddingbot-60 parallax" data-stellar-background-ratio="0.5">

  <div class="carousel-reviews broun-block">
    <div class="container">
      <div class="row">
        <div id="carousel-reviews" class="carousel slide" data-ride="carousel">

          <div class="carousel-inner">
            <div class="item active">
              <div class="col-md-4 col-sm-6">
                <div class="block-text rel zmin">
                  <a title="" href="#">Emergency Contraception</a>
                  <div class="mark">My rating: <span class="rating-input"><span data-value="0" class="glyphicon glyphicon-star"></span><span data-value="1" class="glyphicon glyphicon-star"></span><span data-value="2" class="glyphicon glyphicon-star"></span><span data-value="3"
                      class="glyphicon glyphicon-star"></span><span data-value="4" class="glyphicon glyphicon-star-empty"></span><span data-value="5" class="glyphicon glyphicon-star-empty"></span> </span>
                  </div>
                  <p>Ne eam errem semper. Laudem detracto phaedrum cu vim, pri cu errem fierent fabellas. Quis magna in ius, pro vidit nonumy te, nostrud ...</p>
                  <ins class="ab zmin sprite sprite-i-triangle block"></ins>
                </div>
                <div class="person-text rel text-light">
                  <?php echo $this->Html->image('front/testimonials/1.jpg',['class'=>'person img-circle','alt'=>'']) ?>
                  <a title="" href="#">Anna</a>
                  <span>Chicago, Illinois</span>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 hidden-xs">
                <div class="block-text rel zmin">
                  <a title="" href="#">Orthopedic Surgery</a>
                  <div class="mark">My rating: <span class="rating-input"><span data-value="0" class="glyphicon glyphicon-star"></span><span data-value="1" class="glyphicon glyphicon-star"></span><span data-value="2" class="glyphicon glyphicon-star-empty"></span>
                    <span
                      data-value="3" class="glyphicon glyphicon-star-empty"></span><span data-value="4" class="glyphicon glyphicon-star-empty"></span><span data-value="5" class="glyphicon glyphicon-star-empty"></span> </span>
                  </div>
                  <p>Ne eam errem semper. Laudem detracto phaedrum cu vim, pri cu errem fierent fabellas. Quis magna in ius, pro vidit nonumy te, nostrud ...</p>
                  <ins class="ab zmin sprite sprite-i-triangle block"></ins>
                </div>
                <div class="person-text rel text-light">
                  <?php echo $this->Html->image('front/testimonials/2.jpg',['class'=>'person img-circle','alt'=>'']) ?>
                  <a title="" href="#">Matthew G</a>
                  <span>San Antonio, Texas</span>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 hidden-sm hidden-xs">
                <div class="block-text rel zmin">
                  <a title="" href="#">Medical consultation</a>
                  <div class="mark">My rating: <span class="rating-input"><span data-value="0" class="glyphicon glyphicon-star"></span><span data-value="1" class="glyphicon glyphicon-star"></span><span data-value="2" class="glyphicon glyphicon-star"></span><span data-value="3"
                      class="glyphicon glyphicon-star"></span><span data-value="4" class="glyphicon glyphicon-star"></span><span data-value="5" class="glyphicon glyphicon-star"></span> </span>
                  </div>
                  <p>Ne eam errem semper. Laudem detracto phaedrum cu vim, pri cu errem fierent fabellas. Quis magna in ius, pro vidit nonumy te, nostrud ...</p>
                  <ins class="ab zmin sprite sprite-i-triangle block"></ins>
                </div>
                <div class="person-text rel text-light">
                  <?php echo $this->Html->image('front/testimonials/3.jpg',['class'=>'person img-circle','alt'=>'']) ?>
                  <a title="" href="#">Scarlet Smith</a>
                  <span>Dallas, Texas</span>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="col-md-4 col-sm-6">
                <div class="block-text rel zmin">
                  <a title="" href="#">Birth control pills</a>
                  <div class="mark">My rating: <span class="rating-input"><span data-value="0" class="glyphicon glyphicon-star"></span><span data-value="1" class="glyphicon glyphicon-star"></span><span data-value="2" class="glyphicon glyphicon-star"></span><span data-value="3"
                      class="glyphicon glyphicon-star"></span><span data-value="4" class="glyphicon glyphicon-star-empty"></span><span data-value="5" class="glyphicon glyphicon-star-empty"></span> </span>
                  </div>
                  <p>Ne eam errem semper. Laudem detracto phaedrum cu vim, pri cu errem fierent fabellas. Quis magna in ius, pro vidit nonumy te, nostrud ...</p>
                  <ins class="ab zmin sprite sprite-i-triangle block"></ins>
                </div>
                <div class="person-text rel text-light">
                  <?php echo $this->Html->image('front/testimonials/4.jpg',['class'=>'person img-circle','alt'=>'']) ?>
                  <a title="" href="#">Lucas Thompson</a>
                  <span>Austin, Texas</span>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 hidden-xs">
                <div class="block-text rel zmin">
                  <a title="" href="#">Radiology</a>
                  <div class="mark">My rating: <span class="rating-input"><span data-value="0" class="glyphicon glyphicon-star"></span><span data-value="1" class="glyphicon glyphicon-star"></span><span data-value="2" class="glyphicon glyphicon-star-empty"></span>
                    <span
                      data-value="3" class="glyphicon glyphicon-star-empty"></span><span data-value="4" class="glyphicon glyphicon-star-empty"></span><span data-value="5" class="glyphicon glyphicon-star-empty"></span> </span>
                  </div>
                  <p>Ne eam errem semper. Laudem detracto phaedrum cu vim, pri cu errem fierent fabellas. Quis magna in ius, pro vidit nonumy te, nostrud ...</p>
                  <ins class="ab zmin sprite sprite-i-triangle block"></ins>
                </div>
                <div class="person-text rel text-light">
                  <?php echo $this->Html->image('front/testimonials/5.jpg',['class'=>'person img-circle','alt'=>'']) ?>
                  <a title="" href="#">Ella Mentree</a>
                  <span>Fort Worth, Texas</span>
                </div>
              </div>
              <div class="col-md-4 col-sm-6 hidden-sm hidden-xs">
                <div class="block-text rel zmin">
                  <a title="" href="#">Cervical Lesions</a>
                  <div class="mark">My rating: <span class="rating-input"><span data-value="0" class="glyphicon glyphicon-star"></span><span data-value="1" class="glyphicon glyphicon-star"></span><span data-value="2" class="glyphicon glyphicon-star"></span><span data-value="3"
                      class="glyphicon glyphicon-star"></span><span data-value="4" class="glyphicon glyphicon-star"></span><span data-value="5" class="glyphicon glyphicon-star"></span> </span>
                  </div>
                  <p>Ne eam errem semper. Laudem detracto phaedrum cu vim, pri cu errem fierent fabellas. Quis magna in ius, pro vidit nonumy te, nostrud ...</p>
                  <ins class="ab zmin sprite sprite-i-triangle block"></ins>
                </div>
                <div class="person-text rel text-light">
                  <?php echo $this->Html->image('front/testimonials/6.jpg',['class'=>'person img-circle','alt'=>'']) ?>
                  <a title="" href="#">Suzanne Adam</a>
                  <span>Detroit, Michigan</span>
                </div>
              </div>
            </div>


          </div>

          <a class="left carousel-control" href="#carousel-reviews" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
          <a class="right carousel-control" href="#carousel-reviews" role="button" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
        </div>
      </div>
    </div>
  </div>
</section>
<!--partner--------------->
<section id="partner" class="home-section paddingbot-60">
  <div class="container marginbot-50">
    <div class="row">
      <div class="col-lg-8 col-lg-offset-2">
        <div class="wow lightSpeedIn" data-wow-delay="0.1s">
          <div class="section-heading text-center">
            <h2 class="h-bold">Our partner</h2>
            <p>Take charge of your health today with our specially designed health packages</p>
          </div>
        </div>
        <div class="divider-short"></div>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-sm-6 col-md-3">
        <div class="partner">
          <a href="#"><?php echo $this->Html->image('front/dummy/partner-1.jpg');?></a>
        </div>
      </div>
      <div class="col-sm-6 col-md-3">
        <div class="partner">
          <a href="#"><?php echo $this->Html->image('front/dummy/partner-2.jpg');?></a>
        </div>
      </div>
      <div class="col-sm-6 col-md-3">
        <div class="partner">
          <a href="#"><?php echo $this->Html->image('front/dummy/partner-3.jpg');?></a>
        </div>
      </div>
      <div class="col-sm-6 col-md-3">
        <div class="partner">
          <a href="#"><?php echo $this->Html->image('front/dummy/partner-4.jpg');?></a>
        </div>
      </div>
    </div>
  </div>
</section>
