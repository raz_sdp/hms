<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Medicio landing page template for Health niche</title>
<?php
  //--------------css--------------
  echo $this->Html->css([
          'bootstrap.min',
          'font-awesome.min',
          'front/cubeportfolio.min',
          'front/nivo-lightbox',
          'front/nivo-lightbox-theme/default/default',
          'front/owl.carousel',
          'front/owl.theme',
          'front/animate',
          'front/style',
          'bodybg/bg1',
          'color/default'

  ]);

?>
</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-custom">


  <div id="wrapper">
    <!--********nav section ****-->
    <?php echo $this->element('front/nav'); ?>
    <!--********nav section ****-->

    <!-- Section: intro -->
    <?php
    echo $this->Session->flash();
    echo $this->fetch('content');
    //echo $this->element('front/section-intro'); ?>
    <!-- /Section: intro -->

    <!-- Section: boxes -->
    <?php //echo $this->element('front/section-boxes'); ?>
    <!-- /Section: boxes -->

    <!-- Section: services -->
    <?php //echo $this->element('front/section-services'); ?>
    <!-- /Section: services -->


    <!-- Section: team -->
    <?php echo $this->element('front/section-doctors'); ?>
    <!-- /Section: team -->


    <!-- Section: facilities and testimonial and partner -->
    <?php echo $this->element('front/section-facilities'); ?>
    <!-- /Section: facilities and testimonial and partner -->

    <!--***********footer***********-->
    <?php echo $this->element('front/footer'); ?>
      <!--***********footer***********-->


<?php
//-------js-----------------
  echo $this->Html->script([
          'front/jquery.min',
          'bootstrap.min',
          'front/jquery.easing.min',
          'front/wow.min',
          'front/jquery.scrollTo',
          'front/jquery.appear',
          'front/stellar',
          'front/jquery.cubeportfolio.min',
          'front/owl.carousel.min',
          'front/nivo-lightbox.min',
          'front/custom'
  ]);
 ?>
</body>

</html>
