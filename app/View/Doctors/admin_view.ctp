<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <h3 class="box-title">Doctors Details</h3>
                    </div>
                    <!-- /.box-header -->



                    <!-- /.Search Box -->
										<div class="doctors view">
										<h2><?php echo __('Doctor'); ?></h2>
											<dl>
												<dt><?php echo __('Id'); ?></dt>
												<dd>
													<?php echo h($doctor['Doctor']['id']); ?>
													&nbsp;
												</dd>
												<dt><?php echo __('Name'); ?></dt>
												<dd>
													<?php echo h($doctor['Doctor']['name']); ?>
													&nbsp;
												</dd>
												<dt><?php echo __('Email'); ?></dt>
												<dd>
													<?php echo h($doctor['Doctor']['email']); ?>
													&nbsp;
												</dd>
												<dt><?php echo __('Cell'); ?></dt>
												<dd>
													<?php echo h($doctor['Doctor']['cell']); ?>
													&nbsp;
												</dd>
												<dt><?php echo __('Address'); ?></dt>
												<dd>
													<?php echo h($doctor['Doctor']['address']); ?>
													&nbsp;
												</dd>
												<dt><?php echo __('Appointment'); ?></dt>
												<dd>
													<?php echo h($doctor['Doctor']['appointment']); ?>
													&nbsp;
												</dd>
												<dt><?php echo __('Specialist'); ?></dt>
												<dd>
													<?php echo h($doctor['Doctor']['specialist']); ?>
													&nbsp;
												</dd>
                        <dd>
                          <?php echo $this->Html->image('doctors/'.$doctor['Doctor']['photo'], array('width'=>'240px')); ?>
                          <p>Profile Photo</p>
													&nbsp;
												</dd>

											</dl>
										</div>
										<div class="actions">
											<h3><?php echo __('Actions'); ?></h3>
											<ul>
												<li><?php echo $this->Html->link(__('Edit User'), array('action' => 'edit', $doctor['Doctor']['id'])); ?> </li>
												<li><?php echo $this->Form->postLink(__('Delete User'), array('action' => 'delete', $doctor['Doctor']['id']), array(), __('Are you sure you want to delete # %s?', $doctor['Doctor']['id'])); ?> </li>
												<li><?php echo $this->Html->link(__('List Doctors'), array('action' => 'index')); ?> </li>
												<li><?php echo $this->Html->link(__('New Doctors'), array('action' => 'add')); ?> </li>
											</ul>
										</div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->


                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
</div>
