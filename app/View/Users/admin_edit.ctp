<div class="content-wrapper">
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Edit Admin Details</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->
                    <!--<form role="form">-->
                    <?php echo $this->Form->create('User',array('type'=>'file'));?>
                        <div class="box-body">
                            <div class="form-group">

                                <?php  echo $this->Form->input('name',array('class'=>'form-control','placeholder'=>'Fullname'));?>

                            </div>
                            <div class="form-group">
                                <?php  echo $this->Form->input('email',array('class'=>'form-control','placeholder'=>'Email'));?>
                            </div>
														<div class="form-group">
                                <?php  echo $this->Form->input('password',array('class'=>'form-control','placeholder'=>'Password','type'=>'password'));?>
                            </div>

                    <?php echo $this->Form->end(__('Submit',array('class'=>'btn btn-primary'))); ?>
                   <!-- </form>---->
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
					</div>
					<div class="actions">
						<h3><?php echo __('Actions'); ?></h3>
						<ul>

							<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('User.id')), array('confirm' => __('Are you sure you want to delete # %s?', $this->Form->value('User.id')))); ?></li>
							<li><?php echo $this->Html->link(__('List Admins'), array('action' => 'index')); ?></li>
						</ul>
					</div>
        </div>
        <!--/.col (right) -->
</div>
<!-- /.row -->
</section>
<!-- /.content -->
</div>
