<div class="login-box">
  <div class="login-logo">
    <a href="<?php echo $this->Html->url("/admin", true);?>"><b>Login For Dashboard</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>


    <?php echo $this->Form->create('User');?>
      <div class="form-group has-feedback">
        <input type="email" class="form-control" placeholder="Email" name='data[email]'>
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name='data[password]'>
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox" name='data[role]'> &nbsp;I AM A DOCTOR
            </label>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
      <div style="font-size: 18px; color: #f70606">
        <?php
        echo $this->Flash->render();
        ?>
      </div>
    </form>



    <!-- <a href="#">I forgot my password</a><br>
    <a href="register.html" class="text-center">Register a new membership</a> -->

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->
