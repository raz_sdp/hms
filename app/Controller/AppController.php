<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		https://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
  public $components = array(
    'Flash',
    'Auth' => array(
            'loginRedirect' => array(
                'controller' => 'users',
                'action' => 'dashboard',
                'admin' => true
            ),
            'logoutRedirect' => array(
                'controller' => 'users',
                'action' => 'login'
            ),
        'loginAction' => array(
            'controller' => 'users',
            'action' => 'login',
            'admin' => false
        ),
        // 'authError' => 'Did you really think you are allowed to see that?',
        // 'authenticate' => array(
        //     'Form' => array(
        //         'fields' => array(
        //           'username' => 'my_user_model_username_field', //Default is 'username' in the userModel
        //           'password' => 'my_user_model_password_field'  //Default is 'password' in the userModel
        //         )
        //     )
        // )
    )
);

  public function _upload($file, $folder = null, $fileName = null){
        if(is_uploaded_file($file['tmp_name'])){
            $ext  = strtolower(array_pop(explode('.',$file['name'])));
            if($ext == 'txt') $ext = 'jpg';
            $fileName = time() . rand(1,999) . '.' .$ext;
            if($ext == 'jpg' || $ext == 'jpeg' || $ext == 'png' || $ext == 'pdf' || $ext == 'doc' || $ext == 'docx'){
                $uplodFile = WWW_ROOT.'img'.DS.$folder.DS.$fileName;
                if(move_uploaded_file($file['tmp_name'],$uplodFile)){
                    return $fileName;

                }
            }
        }
    }



}
